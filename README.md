## Provision

- cd provisioning
- make site 
- make authorize
- make docker-login

## Первый запуск
- make up
- make install  
- make migrate
- make import

## Development lifecycle 
- Собрать production контейнеры. Используем хеш коммита в качестве тега
  > make build
- запушить в registry (dockerhub или приватный)
  > make push
- задеплоить контейнеры в Swarm, номер билда на основе даты `date +%d-%m-%Y_%H-%M-%S`
  > make deploy
