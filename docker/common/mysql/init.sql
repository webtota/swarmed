set FOREIGN_KEY_CHECKS = 0;
DROP TABLE IF EXISTS `products`;
CREATE TABLE `products` (
                            `id` int NOT NULL AUTO_INCREMENT,
                            `title` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
                            `description` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
                            `brand` varchar(40) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
                            `price` decimal(10,2) NOT NULL,
                            `img` varchar(512) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
                            `created` datetime NOT NULL,
                            `updated` datetime NOT NULL,
                            `url` varchar(512) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
                            `country_of_origin` varchar(40) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
                            `old_price` decimal(10,2) NOT NULL,
                            `currency` varchar(3) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
                            `sales_notes` varchar(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
                            `foreign_id` varchar(40) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
                            `source_id` int NOT NULL,
                            PRIMARY KEY (`id`),
                            UNIQUE KEY `foreign_id` (`foreign_id`,`source_id`),
                            KEY `source_id` (`source_id`),
                            KEY `old_price` (`old_price`),
                            CONSTRAINT `products_ibfk_1` FOREIGN KEY (`source_id`) REFERENCES `sources` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8_unicode_ci;

DROP TABLE IF EXISTS `products_tags`;
CREATE TABLE `products_tags` (
                                 `product_id` int NOT NULL,
                                 `tag_id` int NOT NULL,
                                 PRIMARY KEY (`product_id`,`tag_id`),
                                 KEY `tag_id` (`tag_id`),
                                 CONSTRAINT `products_tags_ibfk_1` FOREIGN KEY (`product_id`) REFERENCES `products` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
                                 CONSTRAINT `products_tags_ibfk_2` FOREIGN KEY (`tag_id`) REFERENCES `tags` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8_unicode_ci;

DROP TABLE IF EXISTS `sources`;
CREATE TABLE `sources` (
                           `id` int NOT NULL AUTO_INCREMENT,
                           `name` varchar(40) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
                           `title` varchar(100) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
                           `last_start` datetime NOT NULL,
                           `status` tinyint(1) NOT NULL,
                           `feed` varchar(512) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
                           PRIMARY KEY (`id`),
                           UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8_unicode_ci;

DROP TABLE IF EXISTS `tags`;
CREATE TABLE `tags` (
                        `id` int NOT NULL AUTO_INCREMENT,
                        `title` varchar(64) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
                        `slug` varchar(64) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
                        PRIMARY KEY (`id`),
                        UNIQUE KEY `slug` (`slug`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3 COLLATE=utf8_unicode_ci;

INSERT INTO `sources` (`id`, `name`, `title`, `last_start`, `status`, `feed`) VALUES (NULL, '21vek', '21vek.by', '2021-09-14 12:16:00.000000', '1', 'http://export.admitad.com/by/webmaster/websites/1978712/products/export_adv_products/?user=admin%4021.by&code=osoqjn833x&feed_id=6498&format=csv');

set FOREIGN_KEY_CHECKS = 1;