<?php

use yii\db\Migration;

/**
 * Class m210829_170358_tag_level
 */
class m210829_170358_tag_level extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%tags}}', 'level', $this->integer());
        $this->createIndex('idx-tags-level', '{{%tags}}', 'level');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%tags}}', 'level');
    }
}
