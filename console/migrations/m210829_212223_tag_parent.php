<?php

use yii\db\Migration;

/**
 * Class m210829_212223_tag_parent
 */
class m210829_212223_tag_parent extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%tags}}', 'parent_id', $this->integer());
        $this->createIndex('idx-tags-parent', '{{%tags}}', 'parent_id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%tags}}', 'parent_id');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m210829_212223_tag_parent cannot be reverted.\n";

        return false;
    }
    */
}
