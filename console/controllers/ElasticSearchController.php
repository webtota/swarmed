<?php

namespace console\controllers;

use common\models\ElasticProduct;
use yii\console\Controller;

class ElasticSearchController extends Controller
{
    public function actionDeleteIndex()
    {
        ElasticProduct::deleteIndex();
    }
}