<?php

namespace console\controllers;

use common\mappers\Csv;
use common\models\Products;
use common\models\Sources;
use common\models\Tags;
use common\services\Downloader;
use yii\console\Controller;
use yii\helpers\Console;
use yii\helpers\Inflector;

class ImportController extends Controller
{
    public function actionIndex()
    {
        $nextSource = Sources::find()->where(['status'=>1])->orderBy('last_start')->one();
        if (empty($nextSource)) {
            $this->stdout('Nothing to do' . PHP_EOL, Console::FG_RED);
            return;
        }
        $this->actionImportDataFrom($nextSource->id);
    }

    public function actionImportDataFrom(int $sourceId)
    {
        \Yii::getLogger()->flushInterval = 100;
        $source = Sources::find()->where(['id' => $sourceId])->one();
        /**
         * @var Sources $source
         */
        if (empty($source)) {
            throw new \Exception('Unknown source with id ' . $sourceId);
        }
        $csv = \Yii::getAlias('@console') . '/runtime/' . $source->name . '.csv';
        $this->stdout(sprintf('Downloading CSV feed from "%s" to "%s"', $source->feed, $csv) . PHP_EOL, Console::FG_GREEN);
        if (!Downloader::download($source->feed, $csv)) {
            throw new \RuntimeException('Cannot download feed ' . $source->feed);
        }
        $mapper = $this->getMapper($source->name);
        $isHeader = true;
        $this->stdout('Starting import'. PHP_EOL);
        if (($handle = fopen($csv, "r")) !== FALSE) {
            while (($data = fgetcsv($handle, 10000, $mapper->getSeparator())) !== FALSE) {
                if ($isHeader) {
                    $isHeader = false;
                    continue;
                }
                $linkTags = false;
                $mapped = $mapper->map($data);
                if (empty($mapped)) {
                    $this->stdout('Skipped' . PHP_EOL, Console::FG_RED);
                    continue;
                }
                $product = Products::find()->where(['foreign_id'=>$mapped['foreign_id'], 'source_id' => $source->id])->one();
                $tagNames = $mapped['tags'];
                if (empty($product)) {
                    $product = new Products();
                    $linkTags = true;
                }
                unset($mapped['tags']);
                $product->attributes = $mapped;
                $product->source_id = $source->id;
                $this->stdout($product->title . PHP_EOL, Console::FG_CYAN);
                if (!$product->save()) {
                    throw new \Exception('Failed to save product: ' . var_export($product->getErrorSummary(true), true));
                }
                if (!empty($tagNames) && $linkTags) {
                    $tags = Tags::find()->where(['title' => $tagNames])->all();
                    foreach ($tags as $tag) {
                        $product->link('tags', $tag);
                    }
                    unset($tags);
                    unset($tag);
                }
                \Yii::getLogger()->flush(true);
                unset($product);
                gc_collect_cycles();
//                $this->stdout('Memory usage: ' . memory_get_usage(true) . PHP_EOL, Console::FG_RED);
            }
            fclose($handle);
        }
    }

    public function actionImportTagsFrom(int $sourceId)
    {
        \Yii::getLogger()->flushInterval = 100;
        $source = Sources::find()->where(['id' => $sourceId])->one();
        /**
         * @var Sources $source
         */
        if (empty($source)) {
            throw new \Exception('Unknown source with id ' . $sourceId);
        }
        $csv = \Yii::getAlias('@console') . '/runtime/' . $source->name . '.csv';
        $this->stdout(sprintf('Downloading CSV feed from "%s" to "%s"', $source->feed, $csv) . PHP_EOL, Console::FG_GREEN);
//        if (!Downloader::download($source->feed, $csv)) {
//            throw new \RuntimeException('Cannot download feed ' . $source->feed);
//        }
        $mapper = $this->getMapper($source->name);
        $isHeader = true;
        $this->stdout('Starting import'. PHP_EOL);
        if (($handle = fopen($csv, "r")) !== FALSE) {
            while (($data = fgetcsv($handle, 10000, $mapper->getSeparator())) !== FALSE) {
                if ($isHeader) {
                    $isHeader = false;
                    continue;
                }
                $mapped = $mapper->map($data);
                if (empty($mapped)) {
                    $this->stdout('Skipped' . PHP_EOL, Console::FG_RED);
                    continue;
                }
                if (empty($mapped['tags'])) {
                    $this->stdout('No tags found' . PHP_EOL, Console::FG_CYAN);
                }
                $parent_id = null;
                foreach ($mapped['tags'] as $i => $tagName) {
                    $tag = Tags::find()->where(['title' => $tagName])->one();
                    if (empty($tag)) {
                        $this->stdout('New tag ' . $tagName . PHP_EOL, Console::FG_GREEN);
                        $tag = new Tags();
                        $tag->title = $tagName;
                        $tag->slug = Inflector::slug($tagName);
                        $tag->level = $i;
                        $tag->parent_id = $parent_id;
                        $tag->save();
                    } else {
                        $tag->parent_id = $parent_id;
                        $tag->save();
                        $this->stdout('Known tag ' . $tagName . PHP_EOL, Console::FG_CYAN);
                    }
                    $parent_id = $tag->id;
                }
                unset($tag);
                gc_collect_cycles();
            }
            fclose($handle);
        }
    }

    protected function getMapper(string $name): Csv
    {
        $class = '\\common\\mappers\Mapper' . $name;
        if (!class_exists($class)) {
            throw new \Exception('Unknown mapper ' . $class);
        }
        return new $class;
    }

}