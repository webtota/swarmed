# import environment variables
ifneq (,$(wildcard ./.env.prod))
    include .env.prod
    export
endif

IMAGE_TAG=$(shell git rev-parse --short HEAD)
BUILD_NUMBER=$(shell date +%d-%m-%Y_%H-%M-%S)

up:
	docker-compose up -d --remove-orphans

down:
	docker-compose down --remove-orphans

install:
	docker-compose run --rm php-cli composer install

migrate:
	docker-compose run php-cli php yii migrate/up --interactive=0

import:
	docker-compose run php-cli php yii import/index

build:
	docker --log-level=DEBUG build --pull --file=docker/prod/nginx/Dockerfile --tag=${REGISTRY}/${PROJECT_NAME}-webserver:${IMAGE_TAG} .
	docker --log-level=DEBUG build --pull --file=docker/prod/php-cli/Dockerfile --tag=${REGISTRY}/${PROJECT_NAME}-php-cli:${IMAGE_TAG} .
	docker --log-level=DEBUG build --pull --file=docker/prod/php-fpm/Dockerfile --tag=${REGISTRY}/${PROJECT_NAME}-php-fpm:${IMAGE_TAG} .

push:
	docker push ${REGISTRY}/${PROJECT_NAME}-webserver:${IMAGE_TAG}
	docker push ${REGISTRY}/${PROJECT_NAME}-php-cli:${IMAGE_TAG}
	docker push ${REGISTRY}/${PROJECT_NAME}-php-fpm:${IMAGE_TAG}

swarm-import:
#	envsubst < docker-import.yml > docker-import-env.yml
#	scp -o StrictHostKeyChecking=no -P ${SSH_PORT} docker-import-env.yml deploy@${HOST}:site/docker-import.yml
#	ssh -o StrictHostKeyChecking=no deploy@${HOST} -p ${SSH_PORT} 'cd site && docker stack deploy -c docker-import.yml ${PROJECT_NAME} --with-registry-auth --prune'
	#ssh -o StrictHostKeyChecking=no deploy@${HOST} -p ${SSH_PORT} 'cd site && docker secret create mysql_password ./secrets/mysql_password'
	ssh -o StrictHostKeyChecking=no deploy@${HOST} -p ${SSH_PORT} 'cd site && docker service create --network=kukus_frontend --restart-condition=none --with-registry-auth --name import-data2 --env MYSQL_DATABASE=${MYSQL_DATABASE} --env MYSQL_USER=${MYSQL_USER} --env MYSQL_PASSWORD_FILE=/run/secrets/mysql_password --secret mysql_password ${REGISTRY}/${PROJECT_NAME}-php-cli:${IMAGE_TAG} php yii import/index '
	#rm -f docker-import-env.yml

deploy:
	ssh -o StrictHostKeyChecking=no deploy@${HOST} -p ${SSH_PORT} 'rm -rf site_${BUILD_NUMBER}'
	ssh -o StrictHostKeyChecking=no deploy@${HOST} -p ${SSH_PORT} 'mkdir site_${BUILD_NUMBER}'
	envsubst < docker-compose-production.yml > docker-compose-production-env.yml
	scp -o StrictHostKeyChecking=no -P ${SSH_PORT} docker-compose-production-env.yml deploy@${HOST}:site_${BUILD_NUMBER}/docker-compose.yml
	rm -f docker-compose-production-env.yml
	ssh -o StrictHostKeyChecking=no deploy@${HOST} -p ${SSH_PORT} 'mkdir site_${BUILD_NUMBER}/secrets'
	ssh -o StrictHostKeyChecking=no deploy@${HOST} -p ${SSH_PORT} 'mkdir site_${BUILD_NUMBER}/certs'
	ssh -o StrictHostKeyChecking=no deploy@${HOST} -p ${SSH_PORT} 'mkdir site_${BUILD_NUMBER}/config'
	scp -o StrictHostKeyChecking=no -P ${SSH_PORT} docker/prod/secrets/mysql_password deploy@${HOST}:site_${BUILD_NUMBER}/secrets/mysql_password
	scp -o StrictHostKeyChecking=no -P ${SSH_PORT} docker/prod/secrets/mysql_root_password deploy@${HOST}:site_${BUILD_NUMBER}/secrets/mysql_root_password
	scp -o StrictHostKeyChecking=no -P ${SSH_PORT} docker/prod/traefik/certs/prod.pem deploy@${HOST}:site_${BUILD_NUMBER}/certs/prod.pem
	scp -o StrictHostKeyChecking=no -P ${SSH_PORT} docker/prod/traefik/certs/prod-key.pem deploy@${HOST}:site_${BUILD_NUMBER}/certs/prod-key.pem
	scp -o StrictHostKeyChecking=no -P ${SSH_PORT} docker/prod/traefik/config/traefik.yml deploy@${HOST}:site_${BUILD_NUMBER}/config/traefik.yml
	ssh -o StrictHostKeyChecking=no deploy@${HOST} -p ${SSH_PORT} 'cd site_${BUILD_NUMBER} && docker-compose pull'
	ssh -o StrictHostKeyChecking=no deploy@${HOST} -p ${SSH_PORT} 'mkdir site_${BUILD_NUMBER}/mysql'
	scp -o StrictHostKeyChecking=no -P ${SSH_PORT} docker/common/mysql/init.sql deploy@${HOST}:site_${BUILD_NUMBER}/mysql/init.sql
	ssh -o StrictHostKeyChecking=no deploy@${HOST} -p ${SSH_PORT} 'cd site_${BUILD_NUMBER} && docker -D stack deploy --compose-file docker-compose.yml ${PROJECT_NAME} --with-registry-auth --prune'
	ssh -o StrictHostKeyChecking=no deploy@${HOST} -p ${SSH_PORT} 'rm -f site'
	ssh -o StrictHostKeyChecking=no deploy@${HOST} -p ${SSH_PORT} 'ln -sr site_${BUILD_NUMBER} site'