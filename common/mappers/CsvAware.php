<?php

namespace common\mappers;

interface CsvAware
{
    public function getSeparator(): string;
}