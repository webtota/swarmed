<?php

namespace common\mappers;

class Mapper21vek extends Csv
{
    public function map(array $data): array
    {
        $columns = [
            'available',
            'categoryId',
            'country_of_origin',
            'currencyId',
            'delivery_options',
            'description',
            'id',
            'manufacturer_warranty',
            'model',
            'modified_time',
            'name',
            'old_price',
            'param',
            'picture',
            'price',
            'sales_notes',
            'type',
            'typePrefix',
            'url',
            'vendor',
        ];
        $sourceData = array_combine($columns, $data);
        if (empty($sourceData['old_price']) || $sourceData['old_price'] < 0.01) {
         //   return [];
        }
        return [
            'title' => $sourceData['name'],
            'description' => $sourceData['description'],
            'brand' => $sourceData['vendor'],
            'price' => $sourceData['price'],
            'img' => $sourceData['picture'],
            'url' => $sourceData['url'],
            'country_of_origin' => $sourceData['country_of_origin'],
            'old_price' => empty($sourceData['old_price'])? 0 : $sourceData['old_price'],
            'currency' => $sourceData['currencyId'],
            'sales_notes' => $sourceData['sales_notes'],
            'foreign_id' => $sourceData['id'],
            'tags' => explode('/', $sourceData['categoryId']),
        ];
    }

    public function getSeparator(): string
    {
        return ';';
    }
}