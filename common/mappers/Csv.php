<?php

namespace common\mappers;

class Csv implements CsvAware, Mappable
{
    public function getSeparator(): string
    {
        return ',';
    }

    public function map(array $data): array
    {
        return $data;
    }
}