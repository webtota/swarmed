<?php

namespace common\mappers;

interface Mappable
{
    public function map(array $data): array;
}