<?php

namespace common\services;

class Downloader
{
    public static function download($from, $to)
    {
        $fp = @fopen($to, 'w+');
        if (empty($fp)) {
            throw new \RuntimeException("Cannot open file $to for writing");
        }
        $encodedUrl = Url::encodeURI($from);
        $ch = curl_init($encodedUrl);
        curl_setopt($ch, CURLOPT_USERAGENT,
            "Mozilla/5.0 (Macintosh; Intel Mac OS X 10.9; rv:32.0) Gecko/20100101 Firefox/32.0");
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 300);//wait max 5 min
        curl_setopt($ch, CURLOPT_TIMEOUT, 3600);//wait max 1hour
        curl_setopt($ch, CURLOPT_FILE, $fp);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
//        curl_setopt($ch, CURLOPT_COOKIEJAR, '/tmp/cookies.txt');
//        curl_setopt($ch, CURLOPT_COOKIEFILE, '/tmp/cookies.txt');
        $result = curl_exec($ch);
        if ($result === false) {
            throw new \RuntimeException("Can not download file $from. " . curl_error($ch));
        }
        curl_close($ch);
        fclose($fp);
        return $result;
    }


}