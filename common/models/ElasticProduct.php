<?php

namespace common\models;

use yii\elasticsearch\ActiveRecord;
/**
 * @property string $title
 * @property string $description
 * @property string $brand
 * @property int $product_id

 *
 */
class ElasticProduct extends ActiveRecord
{

    public function attributes()
    {
        return [
            'title',
            'description',
            'brand',
            'product_id'
//            'price',
//            'old_price',
//            'sales_notes',
//            'source_id',
//            'created',
//            'updated',
        ];
    }

    /**
     * @return array This model's mapping
     */
    public static function mapping()
    {
        return [
            // Field types: https://www.elastic.co/guide/en/elasticsearch/reference/current/mapping.html#field-datatypes
            'properties' => [
                'title'     => ['type' => 'text'],
                'description'      => ['type' => 'text'],
                'brand'      => ['type' => 'text'],
                'product_id' => ['type' => 'integer']
            ]
        ];
    }

    /**
     * Set (update) mappings for this model
     */
    public static function updateMapping()
    {
        $db = static::getDb();
        $command = $db->createCommand();
        $command->setMapping(static::index(), static::type(), static::mapping());
    }

    /**
     * Create this model's index
     */
    public static function createIndex()
    {
        $db = static::getDb();
        $command = $db->createCommand();
        $command->createIndex(static::index(), [
            //'aliases' => [ /* ... */ ],
            'mappings' => static::mapping(),
            //'settings' => [ /* ... */ ],
        ]);
        $command->createIndex(static::index(), [
            //'aliases' => [ /* ... */ ],
            'mappings' => static::mapping(),
            //'settings' => [ /* ... */ ],
        ]);
    }

    /**
     * Delete this model's index
     */
    public static function deleteIndex()
    {
        $db = static::getDb();
        $command = $db->createCommand();
        $command->deleteIndex(static::index());
    }

    public function getProduct()
    {
        return $this->hasOne(Products::class, ['id' => 'product_id']);
    }
}