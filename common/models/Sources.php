<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "sources".
 *
 * @property int $id
 * @property string $name
 * @property string $title
 * @property string $last_start
 * @property int $status
 * @property string $feed
 *
 * @property Products[] $products
 */
class Sources extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'sources';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['name', 'title', 'last_start', 'status', 'feed'], 'required'],
            [['last_start'], 'safe'],
            [['status'], 'integer'],
            [['name'], 'string', 'max' => 40],
            [['title'], 'string', 'max' => 100],
            [['feed'], 'string', 'max' => 512],
            [['name'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'name' => Yii::t('app', 'Name'),
            'title' => Yii::t('app', 'Title'),
            'last_start' => Yii::t('app', 'Last Start'),
            'status' => Yii::t('app', 'Status'),
            'feed' => Yii::t('app', 'Feed'),
        ];
    }

    /**
     * Gets query for [[Products]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProducts()
    {
        return $this->hasMany(Products::className(), ['source_id' => 'id']);
    }
}
