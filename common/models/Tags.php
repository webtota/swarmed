<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "tags".
 *
 * @property int $id
 * @property string $title
 * @property string $slug
 * @property int $level
 * @property int $parent_id
 *
 * @property Products[] $products
 * @property ProductsTags[] $productsTags
 */
class Tags extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tags';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title', 'slug'], 'required'],
            [['title', 'slug'], 'string', 'max' => 64],
            [['level','parent_id'], 'integer'],
            [['slug'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'title' => Yii::t('app', 'Title'),
            'slug' => Yii::t('app', 'Slug'),
        ];
    }

    /**
     * Gets query for [[Products]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProducts()
    {
        return $this->hasMany(Products::className(), ['id' => 'product_id'])->viaTable('products_tags', ['tag_id' => 'id']);
    }

    /**
     * Gets query for [[ProductsTags]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProductsTags()
    {
        return $this->hasMany(ProductsTags::className(), ['tag_id' => 'id']);
    }
}
