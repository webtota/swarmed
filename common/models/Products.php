<?php

namespace common\models;

use Yii;
use yii\db\ActiveRecord;
use yii\db\Expression;

/**
 * This is the model class for table "products".
 *
 * @property int $id
 * @property string $title
 * @property string $description
 * @property string $brand
 * @property float $price
 * @property string $img
 * @property string $created
 * @property string $updated
 * @property string $url
 * @property string $country_of_origin
 * @property float $old_price
 * @property string $currency
 * @property string $sales_notes
 * @property string $foreign_id
 * @property int $source_id
 *
 * @property ProductsTags[] $productsTags
 * @property Sources $source
 * @property Tags[] $tags
 */
class Products extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'products';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title', 'description', 'brand', 'price', 'img', 'url', 'country_of_origin', 'currency', 'sales_notes', 'foreign_id', 'source_id'], 'required'],
            [['description'], 'string'],
            [['price', 'old_price'], 'number'],
            [['created', 'updated'], 'safe'],
            [['source_id'], 'integer'],
            [['title', 'sales_notes'], 'string', 'max' => 255],
            [['brand', 'country_of_origin', 'foreign_id'], 'string', 'max' => 40],
            [['img', 'url'], 'string', 'max' => 512],
            [['currency'], 'string', 'max' => 3],
            [['foreign_id', 'source_id'], 'unique', 'targetAttribute' => ['foreign_id', 'source_id']],
            [['source_id'], 'exist', 'skipOnError' => true, 'targetClass' => Sources::class, 'targetAttribute' => ['source_id' => 'id']],
        ];
    }

    public function behaviors()
    {
        return [
            [
                'class' => 'yii\behaviors\TimestampBehavior',
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created', 'updated'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated'],
                ],
                'value' => new Expression('NOW()'),
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'title' => Yii::t('app', 'Title'),
            'description' => Yii::t('app', 'Description'),
            'brand' => Yii::t('app', 'Brand'),
            'price' => Yii::t('app', 'Price'),
            'img' => Yii::t('app', 'Img'),
            'created' => Yii::t('app', 'Created'),
            'updated' => Yii::t('app', 'Updated'),
            'url' => Yii::t('app', 'Url'),
            'country_of_origin' => Yii::t('app', 'Country Of Origin'),
            'old_price' => Yii::t('app', 'Old Price'),
            'currency' => Yii::t('app', 'Currency'),
            'sales_notes' => Yii::t('app', 'Sales Notes'),
            'foreign_id' => Yii::t('app', 'Foreign ID'),
            'source_id' => Yii::t('app', 'Source ID'),
        ];
    }

    public function afterSave($insert, $changedAttributes)
    {
        parent::afterSave($insert, $changedAttributes);
        if ($insert) {
            $elasticProduct = new ElasticProduct();
            $elasticProduct->title = $this->title;
            $elasticProduct->description = $this->description;
            $elasticProduct->brand = $this->brand;
            $elasticProduct->product_id = $this->id;
            $elasticProduct->save();
            unset($elasticProduct);
        }
    }

    /**
     * Gets query for [[ProductsTags]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getProductsTags()
    {
        return $this->hasMany(ProductsTags::class, ['product_id' => 'id']);
    }

    /**
     * Gets query for [[Source]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getSource()
    {
        return $this->hasOne(Sources::class, ['id' => 'source_id']);
    }

    /**
     * Gets query for [[Tags]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getTags()
    {
        return $this->hasMany(Tags::class, ['id' => 'tag_id'])->viaTable('products_tags', ['product_id' => 'id']);
    }
}
