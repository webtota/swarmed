<?php

use yii\helpers\Html;
?>
<div class="is-hidden-mobile block">
    <?php if (!empty($parent)):?>
        <h1 class="title is-3"><?=Html::encode($parent->title);?></h1>
    <?php endif;?>
</div>
<?php if (!empty($models)):?>
<div id="topCategories" class="box is-hidden-mobile has-background-dark top-categories ">
<div class="columns is-multiline">
    <?php foreach ($models as $tag): ?>
        <div class="column is-one-quarter">
            <?=Html::a($tag->title, ['site/tag', 'slug' => $tag->slug]);?>
        </div>
    <?php endforeach; ?>
</div>
</div>
<?php endif;?>