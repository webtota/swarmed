<?php

namespace frontend\widgets;

use common\models\Tags;
use yii\base\Widget;

class CategoriesMenu extends Widget
{
    /**
     * @var Tags
     */
    public $parent;

    public function run()
    {
        if (empty($this->parent)) {
            $topCategories = Tags::find()->where(['level' => 0])
                ->orderBy('title ASC')->all();
        } else {
            $topCategories = Tags::find()->where(['parent_id' => $this->parent->id])
                ->orderBy('title ASC')->all();
        }
        return $this->render('categories_menu', ['models' => $topCategories, 'parent' => $this->parent]);
    }
}