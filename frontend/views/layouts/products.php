<?php

/* @var $this \yii\web\View */

/* @var $content string */

use frontend\assets\AppAsset;
use frontend\widgets\CategoriesMenu;
use yii\widgets\Breadcrumbs;
use yii\helpers\Html;
use yii\helpers\Url;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
    <!DOCTYPE html>
    <html lang="<?= Yii::$app->language ?>" class="h-100">
    <head>
        <meta charset="<?= Yii::$app->charset ?>">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <?php $this->registerCsrfMetaTags() ?>
        <title><?= Html::encode($this->title) ?></title>
        <?php $this->head() ?>
    </head>
    <body class="d-flex flex-column h-100">
    <?php $this->beginBody() ?>

    <!-- Main wrapper -->
    <div class="shop-wrapper">

        <!-- Main section -->
        <div id="shop-grid" class="section">
            <!-- Container -->
            <div class="container">
                <nav class="navbar" role="navigation" aria-label="main navigation">
                    <div class="navbar-brand">
                        <a class="navbar-item" href="/">
                            <img src="https://www.21.by/pub/img/skin/main-logo.gif" width="112" height="28">
                            <span class="tag is-success">
                                    Поиск низких цен
                                </span>
                        </a>

                        <a role="button" class="navbar-burger" id="burger" onclick="toggleBurger()" aria-label="menu" aria-expanded="false"
                           data-target="dropdown-menu2">
                            <span aria-hidden="true"></span>
                            <span aria-hidden="true"></span>
                            <span aria-hidden="true"></span>
                        </a>
                    </div>
                </nav>
                <div class="columns category-header">
                    <div class="column main-column is-tablet-landscape-padded">
                        <div class="columns">
                            <div class="column">
                                <form action="<?= Url::to(['site/search']); ?>" autocomplete="off">
                                    <div class="field has-addons">
                                        <div class="control is-expanded">
                                            <input class="input is-medium is-rounded" name="query"
                                                   value="<?= Html::encode(Yii::$app->request->get('query', '')); ?>"
                                                   type="text" placeholder="Я ищу ...">
                                        </div>
                                        <div class="control">
                                            <button type="submit" class="button  is-medium is-rounded is-dark">
                                                Найти
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <?= CategoriesMenu::widget(['parent' => $this->params['parent'] ?? null]); ?>
                <?php if (!empty($this->params['breadcrumbs'])): ?>
                <nav class="breadcrumb">
                    <?= Breadcrumbs::widget([
                        'options' => ['class' => ''],
                        'activeItemTemplate' => '<li class="is-active"><a href="#" aria-current="page">{link}</a></li>',
                        'links' => $this->params['breadcrumbs'],
                    ]) ?>
                </nav>
                <?php endif; ?>
                <?= $content; ?>
            </div>
            <!-- /Container -->
        </div>
        <!-- /Main section -->
    </div>
    <!-- /Main wrapper -->

    <?php $this->endBody() ?>
    <script>
        const toggleBurger = () => {
            let burgerIcon = document.getElementById('burger');
            let dropMenu = document.getElementById('topCategories');
            burgerIcon.classList.toggle('is-active');
            dropMenu.classList.toggle('is-hidden-mobile');
        };
    </script>
    <noindex>
    <!-- Yandex.Metrika counter -->
    <script type="text/javascript" >
        (function(m,e,t,r,i,k,a){m[i]=m[i]||function(){(m[i].a=m[i].a||[]).push(arguments)};
            m[i].l=1*new Date();k=e.createElement(t),a=e.getElementsByTagName(t)[0],k.async=1,k.src=r,a.parentNode.insertBefore(k,a)})
        (window, document, "script", "https://mc.yandex.ru/metrika/tag.js", "ym");

        ym(54552010, "init", {
            clickmap:true,
            trackLinks:true,
            accurateTrackBounce:true,
            webvisor:true,
            ecommerce:"dataLayer"
        });
    </script>
    <noscript><div><img src="https://mc.yandex.ru/watch/54552010" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
    <!-- /Yandex.Metrika counter -->


    <!--LiveInternet counter--><script type="text/javascript"><!--
        document.write("<a href='https://www.liveinternet.ru/click' "+
            "target=_blank><img src='https://counter.yadro.ru/hit?t44.6;r"+
            escape(document.referrer)+((typeof(screen)=="undefined")?"":
                ";s"+screen.width+"*"+screen.height+"*"+(screen.colorDepth?
                screen.colorDepth:screen.pixelDepth))+";u"+escape(document.URL)+
            ";"+Math.random()+
            "' alt='' title='LiveInternet' "+
            "border=0 width=1 height=1></a>")//--></script><!--/LiveInternet-->


    <script type="text/javascript">
        var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
        document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
    </script>
    <script type="text/javascript">
        try {
            var pageTracker = _gat._getTracker("UA-10638306-3");
            pageTracker._addOrganic("tut.by", "query");
            pageTracker._addOrganic("search.tut.by", "query");
            pageTracker._addOrganic("all.by", "query");
            pageTracker._addOrganic("rambler.ru", "words");
            pageTracker._addOrganic("nova.rambler.ru", "query");
            pageTracker._addOrganic("go.mail.ru", "q");
            pageTracker._initData();
            pageTracker._trackPageview();
        } catch(err) {}</script>

    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-87670345-1', 'auto');
        ga('send', 'pageview');

    </script>
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-158175732-1"></script>
    <script>
        window.dataLayer = window.dataLayer || [];
        function gtag(){dataLayer.push(arguments);}
        gtag('js', new Date());

        gtag('config', 'UA-158175732-1');
    </script>

    <!-- Yandex.Metrika counter -->
    <script type="text/javascript">
        var yaParams = {/*Здесь параметры визита*/};
    </script>

    <script src="//mc.yandex.ru/metrika/watch.js" type="text/javascript"></script>
    <script type="text/javascript">
        try { var yaCounter81675 = new Ya.Metrika({id:81675,
            webvisor:true,
            clickmap:true,
            trackLinks:true,
            accurateTrackBounce:true,params:window.yaParams||{ },type:1});
        } catch(e) { }
    </script>
    <noscript><div><img src="//mc.yandex.ru/watch/81675?cnt-class=1" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
    <!-- /Yandex.Metrika counter -->

    </noindex>
    </body>
    </html>
<?php $this->endPage();
