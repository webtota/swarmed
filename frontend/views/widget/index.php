<div id="productSlider" style="width: 100%;">
    <h4><a href="https://market.21.by/">Все товары по низким ценам</a></h4>
    <div id="responsive">
        <?php foreach ($products as $product):?>
            <a href="<?=$product->url;?>" class="productUrl" target="_blank">
                <div>
                    <div class="imgFrame">
                        <img style="" src="<?=$product->img;?>" />
                    </div>
                    <div class="productPrice"><?=Yii::$app->formatter->asCurrency($product->price, $product->currency === 'BYN'? 'RUR' : $product->currency);?></div>
                    <div class="productName"><?=$product->title;?></div>
                </div>
            </a>
        <?php endforeach;?>

    </div>
    <div class="prevButton"></div>
    <div class="nextButton"></div>
</div>
<style type="text/css">
    h4, div {

    }
    #productSlider {
        position: relative;
        font-family: BlinkMacSystemFont, -apple-system, "Segoe UI", "Roboto", "Oxygen", "Ubuntu", "Cantarell", "Fira Sans", "Droid Sans", "Helvetica Neue", "Helvetica", "Arial", sans-serif !important
    }
    #productSlider{
        font-family: Arial,Helvetica,'MS Sans Serif',sans-serif;
    }
    .imgFrame {
        height: 100px;
    }
    .imgFrame:before {
        content: "";
        display: inline-block;
        height: 100%;
        vertical-align: middle;
    }
    .imgFrame img {
        vertical-align: middle;
        max-width: 95px !important;
        max-height: 95px !important;
    }
    #responsive > .tns-item {
        height: 165px;
    }

    .productName {
        font-size: 12px!important;
    }
    .productUrl {
        text-decoration: none;
        text-align: center;
    }
    .productUrl:hover {
        background: #f8f8f8!important;
    }
    .productPrice {
        font-weight: 700!important;
        color: #111;
        text-decoration: none;
        font-size: 16px!important;
        line-height: 20px!important;
    }
    .nextButton{
        position: absolute!important;
        top: calc(50% - 9px)!important;
        right: 10px;
        width: 35px!important;
        height: 35px!important;
        border-radius: 35px!important;
        background-color: #fff!important;
        box-shadow: 0 1px 8px rgba(0,0,0,.06),0 2px 4px rgba(0,0,0,.12)!important;
        transition: box-shadow .15s ease-out!important;
        will-change: box-shadow!important;
    }
    .nextButton:after {
        position: absolute!important;
        top: calc(50% - 9px)!important;
        right: 8px;
        width: 18px!important;
        height: 18px!important;
        content: ""!important;
        background-image: url(data:image/svg+xml;base64,PHN2ZyB2aWV3Qm94PSIwIDAgMjQgMjQiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+PHBhdGggZmlsbC1ydWxlPSJldmVub2RkIiBmaWxsPSIjMzMzIiBkPSJNNCAxM2gxMi4xNzZsLTUuNTgzIDUuNTgzIDEuNDE0IDEuNDE0TDIwLjAwNCAxMmwtNy45OTctNy45OTctMS40MTQgMS40MTRMMTYuMTc2IDExSDR6Ii8+PC9zdmc+)!important;
    }

    .prevButton {
        position: absolute!important;
        top: calc(50% - 9px)!important;
        left: 10px;
        width: 35px!important;
        height: 35px!important;
        border-radius: 35px!important;
        background-color: #fff!important;
        box-shadow: 0 1px 8px rgba(0,0,0,.06),0 2px 4px rgba(0,0,0,.12)!important;
        transition: box-shadow .15s ease-out!important;
        will-change: box-shadow!important;
    }

    .prevButton:after {
        position: absolute!important;
        top: calc(50% - 9px)!important;
        left: 8px;
        width: 18px!important;
        height: 18px!important;
        content: ""!important;
        background-image: url(data:image/svg+xml;base64,PHN2ZyB2aWV3Qm94PSIwIDAgMjQgMjQiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+PHBhdGggZmlsbC1ydWxlPSJldmVub2RkIiBmaWxsPSIjMzMzIiBkPSJNNCAxM2gxMi4xNzZsLTUuNTgzIDUuNTgzIDEuNDE0IDEuNDE0TDIwLjAwNCAxMmwtNy45OTctNy45OTctMS40MTQgMS40MTRMMTYuMTc2IDExSDR6Ii8+PC9zdmc+)!important;
        transform: rotate(
                180deg
        );
    }
</style>
<script type="text/javascript">
    var slider = tns({
        "nav": false,
        "prevButton": ".prevButton",
        "nextButton": ".nextButton",
        "controls": true,
        "responsive": {
            "350": {
                "items": 3,
                // "edgePadding": 30,
            },
            "500": {
                "items": 4,
            },
            "650": {
                "items": 5
            },
            "800": {
                "items": 7
            }
        },
        slideBy: 1,
        navPosition: "bottom",
        "container": "#responsive",
        "swipeAngle": false,
        "speed": 400,
        // "autoplay": true
    });
</script>
