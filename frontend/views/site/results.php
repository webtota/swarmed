<?php

use kop\y2sp\ScrollPager;
use yii\widgets\ListView;
$this->title = Yii::t('app', 'Search results "{query}"', ['query'=> Yii::$app->request->get('query', '')]);
$products = $dataProvider->getModels();
?>
<div class="list-view">
    <?php if (empty($products)):?>
    <p class="notification is-warning"><?=Yii::t('app', 'Nothing found');?></p>
    <?php else:?>
    <?php foreach (array_chunk($products, 2) as $pair): ?>
        <div class="item">
            <div class="columns">
                <?php foreach ($pair as $r): ?>
                    <div class="column is-6">
                        <div class="box">
                            <a href="<?= $r->product->url; ?>">
                                <article class="media">
                                    <div class="media-left">
                                        <figure class="image is-128x128">
                                            <img src="<?= $r->product->img; ?>" alt="<?= $r->product->title; ?>"/>
                                        </figure>
                                    </div>
                                    <div class="media-content">
                                        <div class="content">
                                            <h3><?= $r->product->title; ?></h3>
                                            <p><?= $r->product->description; ?></p>
                                        </div>
                                        <div class="productPrice">
                                    <span class="is-warning is-light button">
                                    <?=Yii::$app->formatter->asCurrency($r->product->price, $r->product->currency === 'BYN'? 'RUR' : $r->product->currency);?>
                                    </span>
                                            <?php if ($r->product->old_price > 0):?>
                                                <span class=" is-danger is-light button" style="text-decoration: line-through">
                                    <?=Yii::$app->formatter->asCurrency($r->product->old_price, $r->product->currency === 'BYN'? 'RUR' : $r->product->currency);?>
                                    </span>
                                            <?php endif;?>
                                        </div>
                                    </div>
                                </article>
                            </a>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
    <?php endforeach; ?>
    <?= ScrollPager::widget([
        'pagination' => $dataProvider->getPagination(),
        'triggerTemplate' => <<<'HTML'
<div class="ias-trigger" style="text-align: center; cursor: pointer;"><a class="button is-medium is-fullwidth is-dark">{text}</a></div>
HTML
    ]) ?>
    <?php endif;?>
</div>

