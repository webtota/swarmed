<div class="column is-6">
    <div class="box">
        <a href="<?= $model->url; ?>">
            <article class="media">
                <div class="media-left">
                    <figure class="image is-128x128">
                        <img src="<?= $model->img; ?>" alt="<?= $model->title; ?>"/>
                    </figure>
                </div>
                <div class="media-content">
                    <div class="content">
                        <h3><?= $model->title; ?></h3>
                        <p><?= $model->description; ?></p>
                    </div>
                    <div class="productPrice">
                        <?=Yii::$app->formatter->asCurrency($model->price, $model->currency === 'BYN'? 'RUR' : $model->currency);?>
                    </div>
                </div>
            </article>
        </a>
    </div>
</div>