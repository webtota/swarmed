<?php

use common\models\Tags;

$this->params['parent'] = $tag;
if ($tag->level) {
    $parentId = $tag->parent_id;
    while ($parentTag = Tags::find()->where(['id' => $parentId])->one()) {
        $this->params['breadcrumbs'][] = [
            'label' => $parentTag->title,
            'url' => ['site/tag', 'slug' => $parentTag->slug]
        ];
        $parentId = $parentTag->parent_id;
    }
    $this->params['breadcrumbs'] = array_reverse($this->params['breadcrumbs']);
}

$this->params['breadcrumbs'][] = $tag->title;
echo $this->render('_list', ['dataProvider' => $dataProvider]);