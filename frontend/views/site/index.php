<?php
$this->title = Yii::t('app', 'Best products');
?>

<?php foreach (array_chunk($products, 2) as $pair): ?>
    <div class="columns">
        <?php foreach ($pair as $product): ?>
            <div class="column is-6">
                <div class="box">
                    <a href="<?= $product->url; ?>">
                        <article class="media">
                            <div class="media-left">
                                <figure class="image is-128x128">
                                    <img src="<?= $product->img; ?>" alt="<?= $product->title; ?>"/>
                                </figure>
                            </div>
                            <div class="media-content">
                                <div class="content">
                                    <h3><?= $product->title; ?></h3>
                                    <p><?= $product->description; ?></p>
                                </div>
                                <div class="productPrice">
                                    <?=Yii::$app->formatter->asCurrency($product->price, $product->currency === 'BYN'? 'RUR' : $product->currency);?>
                                </div>
                            </div>
                        </article>
                    </a>
                </div>
            </div>
        <?php endforeach; ?>
    </div>
<?php endforeach; ?>
<!-- /Product grid -->

<div class="show-more"><a href="#">Show more products</a></div>
