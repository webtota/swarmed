<?php

use kop\y2sp\ScrollPager;
use yii\widgets\ListView;

$products = $dataProvider->getModels();
?>
<div class="list-view">
    <?php foreach (array_chunk($products, 2) as $pair): ?>
        <div class="item">
            <div class="columns">
                <?php foreach ($pair as $product): ?>
                    <div class="column is-6">
                        <div class="box">
                            <a href="<?= $product->url; ?>">
                                <article class="media">
                                    <div class="media-left">
                                        <figure class="image is-128x128">
                                            <img src="<?= $product->img; ?>" alt="<?= $product->title; ?>"/>
                                        </figure>
                                    </div>
                                    <div class="media-content">
                                        <div class="content">
                                            <h3><?= $product->title; ?></h3>
                                            <p><?= $product->description; ?></p>
                                        </div>
                                        <div class="productPrice">
                                    <span class="is-warning is-light button">
                                    <?=Yii::$app->formatter->asCurrency($product->price, $product->currency === 'BYN'? 'RUR' : $product->currency);?>
                                    </span>
                                            <?php if ($product->old_price > 0):?>
                                                <span class=" is-danger is-light button" style="text-decoration: line-through">
                                    <?=Yii::$app->formatter->asCurrency($product->old_price, $product->currency === 'BYN'? 'RUR' : $product->currency);?>
                                    </span>
                                            <?php endif;?>
                                        </div>
                                    </div>
                                </article>
                            </a>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
    <?php endforeach; ?>
    <?= ScrollPager::widget([
        'pagination' => $dataProvider->getPagination(),
        'triggerTemplate' => <<<'HTML'
<div class="ias-trigger" style="text-align: center; cursor: pointer;"><a class="button is-medium is-fullwidth is-dark">{text}</a></div>
HTML
    ]) ?>
</div>

