<?php

namespace frontend\controllers;

use common\models\Products;
use yii\db\Query;
use yii\web\Controller;

class WidgetController extends Controller
{
    public $layout = 'widget';

    public function actionIndex()
    {
        $products = $this->getProducts();
        return $this->render('index', ['products' => $products]);
    }

    /**
     * @return array|\yii\db\ActiveRecord[]
     */
    protected function getProducts(): array
    {
        $minId = (new Query())->select('min(id)')->from('products')->scalar();
        $maxId = (new Query())->select('max(id)')->from('products')->scalar();

//        $ids = [];
//        for ($i = 0; $i < 20; $i++) {
//            $ids[] = rand($minId, $maxId);
//        }
        $rand = rand($minId, $maxId);
        $products = Products::find()->where('id > :id and old_price > 0', [':id' => $rand])->orderBy('id')->limit(20)->all();
        if (count($products) < 10) {
            $products = Products::find()->where('id < :id and old_price > 0', [':id' => $rand])->orderBy('id')->limit(20)->all();
        }
        return $products;
    }
}